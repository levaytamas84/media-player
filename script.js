const video = document.getElementById('video');
const play = document.getElementById('play');
const stop = document.getElementById('stop');
const progress = document.getElementById('progress');
const timestamp = document.getElementById('timestamp');
const fw = document.getElementById('stepfw');
const bw = document.getElementById('stepbw');
const fullscreen = document.getElementById('fullscreen');

const videos = ['180', 'fidesz', 'holysub', 'scatman', 'shgoa', 'vote', 'holy', 'shakira'];

// var playPromise = document.querySelector('video').play();

// In browsers that don’t yet support this functionality,
// playPromise won’t be defined.
// if (playPromise !== undefined) {
//   playPromise
//     .then(function () {
//       toggleVideoStatus;
//     })
//     .catch(function (error) {
//       console.log('press start');
//     });
// }

let videoIndex = 0;

loadVideo(videos[videoIndex]);

function loadVideo(movie) {
  video.src = `videos/${movie}.mp4`;
}

function nextVideo() {
  videoIndex++;
  if (videoIndex <= videos.length - 1) {
    loadVideo(videos[videoIndex]);
    video.play();
  } else {
    videoIndex = 0;
    loadVideo(videos[videoIndex]);
    video.play();
  }
}

function prevVideo() {
  let tmp = 10;
  tmp = videoIndex - 1;
  if (tmp <= 0) {
    tmp = videos.length - 1;
  }
  loadVideo(videos[tmp]);
  videoIndex = tmp;
  //video.play();
}

function prevVideo_() {
  videoIndex--;
  if (videoIndex <= videos.length - 1) {
    loadVideo(videos[videoIndex]);
    video.play();
  } else {
    videoIndex = videos.length - 1;
    loadVideo(videos[videoIndex]);
    video.play();
  }
}

function toggleVideoStatus() {
  if (video !== 'undefined') {
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  }
}

function updatePlayIcon() {
  if (video.paused) {
    play.innerHTML = '<i class="fa fa-play fa-2x"></i>';
  } else {
    play.innerHTML = '<i class="fa fa-pause fa-2x"></i>';
  }
}

function updateProgress() {
  progress.value = (video.currentTime / video.duration) * 100;

  let mins = Math.floor(video.currentTime / 60);
  if (mins < 10) {
    mins = '0' + String(mins);
  }

  let secs = Math.floor(video.currentTime % 60);

  if (secs < 10) {
    secs = '0' + String(secs);
  }

  timestamp.innerHTML = `${mins}:${secs}`;
}

function setVideoProgress() {
  video.currentTime = (+progress.value * video.duration) / 100;
}

function stopVideo() {
  video.currentTime = 0;
  video.pause();
}

function openFullscreen() {
  if (video.requestFullscreen) {
    video.requestFullscreen();
  } else if (video.webkitRequestFullscreen) {
    /* Safari */
    video.webkitRequestFullscreen();
  } else if (video.msRequestFullscreen) {
    /* IE11 */
    video.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) {
    /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE11 */
    document.msExitFullscreen();
  }
}

video.addEventListener('click', toggleVideoStatus);
video.addEventListener('pause', updatePlayIcon);
video.addEventListener('play', updatePlayIcon);
video.addEventListener('timeupdate', updateProgress);
play.addEventListener('click', toggleVideoStatus);
stop.addEventListener('click', stopVideo);
video.addEventListener('ended', nextVideo);
fw.addEventListener('click', nextVideo);
bw.addEventListener('click', prevVideo);
fullscreen.addEventListener('click', openFullscreen);

progress.addEventListener('change', setVideoProgress);
